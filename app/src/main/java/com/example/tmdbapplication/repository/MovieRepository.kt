package com.example.tmdbapplication.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.tmdbapplication.api.RetrofitClient
import com.example.tmdbapplication.model.Movie
import com.example.tmdbapplication.response.MovieResponse
import com.example.tmdbapplication.roomdatabase.ApiMovieDataDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieRepository (private val apiKey: String,private val apiMovieDataDao : ApiMovieDataDao){
    private var movieList: MutableLiveData<List<Movie>> = MutableLiveData()

    private fun getMovie(): MutableLiveData<List<Movie>> {
        val call= RetrofitClient.apiInterface.getMovies(apiKey)
        call.enqueue(object : Callback<MovieResponse> {
            override fun onResponse(
                call : Call<MovieResponse>,
                response : Response<MovieResponse>,
            ) {
                if (response.isSuccessful){
                    val body=response.body()
                    movieList.postValue(body?.movieList)
                    CoroutineScope(Dispatchers.Main).launch {
                        body?.movieList.let {
                            for (movie in it!!)
                                apiMovieDataDao.saveMovieTODb(movie)
                        }
                    }
                }
            }
            override fun onFailure(call : Call<MovieResponse>, t : Throwable) {
            }

        })
        return movieList
    }
    private fun getallmovietodb():List<Movie>{
        return apiMovieDataDao.getAllMovieToLocalDb()
    }
    suspend fun getmovie():LiveData<List<Movie>> {
        var getdata:List<Movie>?=null
        val job=CoroutineScope(Dispatchers.IO).async{
            getdata=getallmovietodb()
            return@async getdata
        }
        if (job.await()!!.isNotEmpty()){
            movieList.postValue(getdata!!)

        }
        else{
            movieList = getMovie()
        }
        return movieList
    }
}