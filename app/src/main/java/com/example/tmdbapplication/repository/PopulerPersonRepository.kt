package com.example.tmdbapplication.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.tmdbapplication.api.RetrofitClient
import com.example.tmdbapplication.model.PopulerPersonObject
import com.example.tmdbapplication.response.PopulerPersonResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PopulerPersonRepository(private val apiKey:String){
        val tag:String=" Response Result "
        private var populerPersonList: MutableLiveData<List<PopulerPersonObject>> = MutableLiveData()

        fun getPopulerPerson(): LiveData<List<PopulerPersonObject>> {
            val call= RetrofitClient.apiInterface.getPopulerPerson(apiKey)
            call.enqueue(object :Callback<PopulerPersonResponse>{
                override fun onResponse(
                    call : Call<PopulerPersonResponse>,
                    response : Response<PopulerPersonResponse>,
                ) {
                    if (response.isSuccessful){
                        val body=response.body()
                        populerPersonList.postValue(body?.populerpersonList)
                    }
                }

                override fun onFailure(call : Call<PopulerPersonResponse>, t : Throwable) {
                    Log.d(tag, "onFailure: ${t.message}")
                }

            })
            return populerPersonList
        }
}