package com.example.tmdbapplication.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.tmdbapplication.api.RetrofitClient
import com.example.tmdbapplication.model.TvShowItems
import com.example.tmdbapplication.response.TvShowsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TvShowsRepository (private val apiKey:String){
    private var tvShowsList: MutableLiveData<List<TvShowItems>> = MutableLiveData()

    fun getTvShows(): LiveData<List<TvShowItems>> {
        val call= RetrofitClient.apiInterface.getTvShows(apiKey)
        call.enqueue(object :Callback<TvShowsResponse>{
            override fun onResponse(
                call : Call<TvShowsResponse>,
                response : Response<TvShowsResponse>,
            ) {
                if (response.isSuccessful){
                    val body=response.body()
                    tvShowsList.postValue(body?.tvShowsList)
                }
            }

            override fun onFailure(call : Call<TvShowsResponse>, t : Throwable) {

            }

        })
        return tvShowsList
    }
}