package com.example.tmdbapplication.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.tmdbapplication.repository.PopulerPersonRepository
import com.example.tmdbapplication.viewModel.PopulerPersonActivityViewModel

@Suppress("UNCHECKED_CAST")
class PopulerPersonActivityFactory(private val populerPersonRepository : PopulerPersonRepository):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass : Class<T>) : T {
        if (modelClass.isAssignableFrom(PopulerPersonActivityViewModel::class.java)){
            return PopulerPersonActivityViewModel(populerPersonRepository) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}