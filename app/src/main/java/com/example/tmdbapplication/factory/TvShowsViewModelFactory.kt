package com.example.tmdbapplication.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.tmdbapplication.repository.TvShowsRepository
import com.example.tmdbapplication.viewModel.TvShowsViewModel

@Suppress("UNCHECKED_CAST")
class TvShowsViewModelFactory(private val tvShowsRepository : TvShowsRepository):
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass : Class<T>) : T {
        if (modelClass.isAssignableFrom(TvShowsViewModel::class.java)){
            return TvShowsViewModel(tvShowsRepository) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}