package com.example.tmdbapplication.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.tmdbapplication.repository.MovieRepository
import com.example.tmdbapplication.viewModel.PopulerMovieActivityViewModel

@Suppress("UNCHECKED_CAST")
class PopulerMovieActivityFactory(private val movieRepository : MovieRepository):
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass : Class<T>) : T {
        if (modelClass.isAssignableFrom(PopulerMovieActivityViewModel::class.java)){
            return PopulerMovieActivityViewModel(movieRepository) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}