package com.example.tmdbapplication.model

import com.google.gson.annotations.SerializedName

data class KnownForObjectItems(
    @SerializedName("adult")
    var adult:Boolean,

    @SerializedName("backdrop_path")
    var backdropPath:String,

    @SerializedName("id")
    var id:Int,

    @SerializedName("media_type")
    var mediaType:String,

    @SerializedName("original_language")
    var originalLanguage:String,

    @SerializedName("original_title")
    var originalTitle:String,

    @SerializedName("overview")
    var overview:String,

    @SerializedName("poster_path")
    var posterPath:String,

    @SerializedName("release_date")
    var releaseData:String,

    @SerializedName("title")
    var title:String,

    @SerializedName("vote_average")
    var rating:Double
)
