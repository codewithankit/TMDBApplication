package com.example.tmdbapplication.model

import com.google.gson.annotations.SerializedName

data class TvShowItems(
    @SerializedName("backdrop_path")
    var backdropPath:String,

    @SerializedName("first_air_date")
    var firstAirDate:String,

    @SerializedName("name")
    var name:String,

    @SerializedName("original_language")
    var originalLanguage:String,

    @SerializedName("original_name")
    var originalName:String,

    @SerializedName("overview")
    var overView:String,

    @SerializedName("popularity")
    var popularity:Double,

    @SerializedName("poster_path")
    var posterPath:String,

    @SerializedName("vote_average")
    var rating:Double
)
