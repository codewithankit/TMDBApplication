package com.example.tmdbapplication.model

import com.google.gson.annotations.SerializedName

data class PopulerPersonObject(
    @SerializedName("adult")
    var adult:Boolean,

    @SerializedName("gender")
    var gender:Int,

    @SerializedName("known_for")
    var knownFor:ArrayList<KnownForObjectItems>,

    @SerializedName("known_for_department")
    var knownForDepartment:String,

    @SerializedName("name")
    var name:String,

    @SerializedName("popularity")
    var popularity:Double,

    @SerializedName("profile_path")
    var profilePath:String
)
