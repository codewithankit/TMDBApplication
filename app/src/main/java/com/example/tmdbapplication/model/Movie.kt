package com.example.tmdbapplication.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Movie(
    @PrimaryKey(autoGenerate = true)
    var SrNo:Int=0,

    @SerializedName("backdrop_path")
    var backdropPath:String,

    @SerializedName("original_language")
    var language:String,

    @SerializedName("original_title")
    var originalTitle:String,

    @SerializedName("overview")
    var overView:String,

    @SerializedName("popularity")
    var popularity:String,

    @SerializedName("release_date")
    var releaseData:String,

    @SerializedName("poster_path")
    var posterPath:String,

    @SerializedName("title")
    var title:String,

    @SerializedName("vote_average")
    var rating:String
)
