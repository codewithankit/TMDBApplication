package com.example.tmdbapplication.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.tmdbapplication.model.TvShowItems
import com.example.tmdbapplication.repository.TvShowsRepository

class TvShowsViewModel(private val tvShowsRepository : TvShowsRepository): ViewModel()  {
    fun getTvShow(): LiveData<List<TvShowItems>> {
        return tvShowsRepository.getTvShows()
    }
}