package com.example.tmdbapplication.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.tmdbapplication.model.PopulerPersonObject
import com.example.tmdbapplication.repository.PopulerPersonRepository

class PopulerPersonActivityViewModel(private val populerPersonRepository : PopulerPersonRepository): ViewModel()  {
    fun getPopulerPerson(): LiveData<List<PopulerPersonObject>> {
        return populerPersonRepository.getPopulerPerson()
    }
}