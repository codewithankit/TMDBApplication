package com.example.tmdbapplication.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.tmdbapplication.model.Movie
import com.example.tmdbapplication.repository.MovieRepository


class PopulerMovieActivityViewModel(
    private val movieRepository : MovieRepository
): ViewModel() {

    suspend fun getPopulerMovies(): LiveData<List<Movie>> {
        return movieRepository.getmovie()
    }
//    suspend fun getPopulerMovies(): LiveData<List<Movie>> {
//        return movieRepository.getmovie()
//    }

}