package com.example.tmdbapplication.response

import com.example.tmdbapplication.model.TvShowItems
import com.google.gson.annotations.SerializedName

class TvShowsResponse {
    @SerializedName("results")
    val tvShowsList:ArrayList<TvShowItems> =ArrayList()
}