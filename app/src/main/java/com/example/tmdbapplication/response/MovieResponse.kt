package com.example.tmdbapplication.response

import com.example.tmdbapplication.model.Movie
import com.google.gson.annotations.SerializedName

class MovieResponse {
    @SerializedName("results")
    val movieList:ArrayList<Movie> =ArrayList()
}