package com.example.tmdbapplication.response

import com.example.tmdbapplication.model.PopulerPersonObject
import com.google.gson.annotations.SerializedName

class PopulerPersonResponse {
    @SerializedName("results")
    val populerpersonList:ArrayList<PopulerPersonObject> =ArrayList()
}