package com.example.tmdbapplication.utility

class Keys {
    companion object{
        const val MOVIEOBJECT="movie"
        const val POPULERPERSON = "populerpersonobject"
        const val TVSHOWKEY="tvshowitems"
    }
}