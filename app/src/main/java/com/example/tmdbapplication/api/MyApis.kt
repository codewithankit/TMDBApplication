package com.example.tmdbapplication.api

import com.example.tmdbapplication.response.MovieResponse
import com.example.tmdbapplication.response.PopulerPersonResponse
import com.example.tmdbapplication.response.TvShowsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MyApis {
    @GET("/3/movie/popular")
    fun getMovies(@Query("api_key")apiKey:String): Call<MovieResponse>

    @GET("/3/person/popular")
    fun getPopulerPerson(@Query("api_key")apiKey : String):Call<PopulerPersonResponse>

    @GET("/3/tv/popular")
    fun getTvShows(@Query("api_key")apiKey : String):Call<TvShowsResponse>
}