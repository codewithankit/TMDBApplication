package com.example.tmdbapplication.roomdatabase

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.tmdbapplication.model.Movie

@Dao
interface ApiMovieDataDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveMovieTODb(vararg movieList: Movie)


    @Query("SELECT*FROM Movie")
    fun getAllMovieToLocalDb(): List<Movie>




    //    @Query("SELECT * FROM PopularMovies_Model")
//    fun getmoviefromlocaldb():MutableLiveData<List<PopularMovies_Model>>
//    @Query("SELECT * FROM PopularMovies_Model")
//    fun getmoviefromlocaldb(): List<PopularMovies_Model>

}