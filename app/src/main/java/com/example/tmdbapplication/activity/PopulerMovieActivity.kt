package com.example.tmdbapplication.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tmdbapplication.R
import com.example.tmdbapplication.adapter.MyPopulerMovieRecyclerAdapter
import com.example.tmdbapplication.databinding.ActivityPopulerMovieBinding
import com.example.tmdbapplication.factory.PopulerMovieActivityFactory
import com.example.tmdbapplication.repository.MovieRepository
import com.example.tmdbapplication.roomdatabase.MovieDatabase
import com.example.tmdbapplication.viewModel.PopulerMovieActivityViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PopulerMovieActivity : AppCompatActivity(){
    private lateinit var binding:ActivityPopulerMovieBinding
    private lateinit var viewModel:PopulerMovieActivityViewModel
    private lateinit var factory: PopulerMovieActivityFactory
    private lateinit var adapter:MyPopulerMovieRecyclerAdapter
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_populer_movie)
        factory= PopulerMovieActivityFactory(MovieRepository("4da2ce878649bbad5b1ad09442ef2095",
            MovieDatabase.getDatabase(this).getMovieDao()))
        viewModel= ViewModelProvider(this,factory)[PopulerMovieActivityViewModel::class.java]

        binding.RecyclerView.layoutManager= GridLayoutManager(this,2)
        CoroutineScope(Dispatchers.Main).launch {
            viewModel.getPopulerMovies().observe(this@PopulerMovieActivity) {
                adapter = MyPopulerMovieRecyclerAdapter(it, this@PopulerMovieActivity)
                binding.viewProgressbar.isVisible = false
                binding.RecyclerView.adapter = adapter
                binding.notifyChange()
            }
        }
    }
}