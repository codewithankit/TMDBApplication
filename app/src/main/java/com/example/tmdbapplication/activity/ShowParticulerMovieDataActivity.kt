package com.example.tmdbapplication.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.tmdbapplication.R
import com.example.tmdbapplication.databinding.ActivityShowParticulerMovieDataBinding
import com.example.tmdbapplication.model.Movie
import com.example.tmdbapplication.utility.Keys
import com.google.gson.Gson
import com.squareup.picasso.Picasso

class ShowParticulerMovieDataActivity : AppCompatActivity() {
    private lateinit var binding:ActivityShowParticulerMovieDataBinding
    private lateinit var movieData:Movie
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil.setContentView(this,R.layout.activity_show_particuler_movie_data)
        val tempIntent=intent
        movieData= Gson().fromJson(tempIntent.getStringExtra(Keys.MOVIEOBJECT),Movie::class.java)
        setDataFields(movieData)
    }

    private fun setDataFields(movieData:Movie) {
     binding.showMovieTitle.text=movieData.originalTitle
     binding.showMovieLanguage.text=movieData.language
     binding.showMovieReleasingDate.text=movieData.releaseData
     binding.showMovieRating.text=movieData.rating
     binding.showMovieOverview.text=movieData.overView
        val urlPath="https://image.tmdb.org/t/p/w500/${movieData.posterPath}"
        Picasso.get()
            .load(urlPath)
            .placeholder(R.drawable.baseline_movie_24)
            .error(R.drawable.baseline_error_24)
            .into(binding.imageMovie)
    }
}