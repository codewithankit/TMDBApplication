package com.example.tmdbapplication.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.tmdbapplication.R
import com.example.tmdbapplication.databinding.ActivityPersonViewInfoBinding
import com.example.tmdbapplication.model.PopulerPersonObject
import com.example.tmdbapplication.utility.Keys
import com.google.gson.Gson
import com.squareup.picasso.Picasso

class PersonViewInfoActivity : AppCompatActivity() {
    private lateinit var binding:ActivityPersonViewInfoBinding
    private lateinit var personData:PopulerPersonObject
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil.setContentView(this,R.layout.activity_person_view_info)
        val tempIntent=intent
        personData= Gson().fromJson(tempIntent.getStringExtra(Keys.POPULERPERSON),PopulerPersonObject::class.java)
        setDataFields(personData)
    }
    private fun setDataFields(personData:PopulerPersonObject) {
       binding.showPersonName.text=personData.name
        binding.showPersonKnownfordipartment.text=personData.knownForDepartment
        binding.showPopulerPersonReleasingDate.text=personData.knownFor.listIterator().next().releaseData
        binding.showPersonRating.text=personData.knownFor.listIterator().next().releaseData
        binding.showPersonRating.text=personData.knownFor.listIterator().next().rating.toString()
        binding.showPersonOverview.text=personData.knownFor.listIterator().next().overview
        binding.showPersonTitle.text=personData.knownFor.listIterator().next().originalTitle
        binding.showPersonPopulerity.text=personData.popularity.toString()
        val urlPath="https://image.tmdb.org/t/p/w500/${personData.profilePath}"
        Picasso.get()
            .load(urlPath)
            .placeholder(R.drawable.baseline_movie_24)
            .error(R.drawable.baseline_error_24)
            .into(binding.imagePerson)
    }
}