package com.example.tmdbapplication.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.tmdbapplication.R
import com.example.tmdbapplication.databinding.ActivityShowParticulerTvShowDataBinding
import com.example.tmdbapplication.model.TvShowItems
import com.example.tmdbapplication.utility.Keys
import com.google.gson.Gson
import com.squareup.picasso.Picasso

class ShowParticulerTvShowDataActivity : AppCompatActivity() {
    private lateinit var binding:ActivityShowParticulerTvShowDataBinding
    private lateinit var tvShowData:TvShowItems
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_show_particuler_tv_show_data)

        val tempIntent=intent
        tvShowData= Gson().fromJson(tempIntent.getStringExtra(Keys.TVSHOWKEY), TvShowItems::class.java)
        setDataFields(tvShowData)
    }
    private fun setDataFields(tvShowData:TvShowItems) {
        binding.showTvShowName.text=tvShowData.name

        binding.showTvShowReleasingDate.text=tvShowData.firstAirDate
        binding.showTvShowRating.text=tvShowData.rating.toString()
        binding.showTvShowPopularity.text=tvShowData.popularity.toString()
        binding.showTvShowLanguage.text=tvShowData.originalLanguage
        binding.showTvShowOverview.text=tvShowData.overView
        val urlPath="https://image.tmdb.org/t/p/w500/${tvShowData.posterPath}"
        Picasso.get()
            .load(urlPath)
            .placeholder(R.drawable.baseline_movie_24)
            .error(R.drawable.baseline_error_24)
            .into(binding.imageTvShowPoster)
    }
}