package com.example.tmdbapplication.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.tmdbapplication.R
import com.example.tmdbapplication.databinding.ActivityDeshbordBinding

class DeshbordActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityDeshbordBinding
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this,R.layout.activity_deshbord)
        binding.btnPopulerMovies.setOnClickListener(this)
        binding.btnPopulerPerson.setOnClickListener(this)
        binding.btnPopulerShows.setOnClickListener(this)
    }

    override fun onClick(view : View?) {
        when(view?.id){
            R.id.btn_populer_movies->{
             val intent=Intent(this@DeshbordActivity,PopulerMovieActivity::class.java)
                startActivity(intent)
            }
            R.id.btn_populer_person->{
             startActivity(Intent(this@DeshbordActivity,PopulerPersonActivity::class.java))
            }
            R.id.btn_populer_shows->{
             startActivity(Intent(this@DeshbordActivity,TvShowsActivity::class.java))
            }

        }
    }
}