package com.example.tmdbapplication.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tmdbapplication.R
import com.example.tmdbapplication.adapter.MyTvShowsItemsRecyclerViewAdapter
import com.example.tmdbapplication.databinding.ActivityTvShowsBinding
import com.example.tmdbapplication.factory.TvShowsViewModelFactory
import com.example.tmdbapplication.repository.TvShowsRepository
import com.example.tmdbapplication.viewModel.TvShowsViewModel

class TvShowsActivity : AppCompatActivity() {
    private lateinit var binding:ActivityTvShowsBinding
    private lateinit var factory:TvShowsViewModelFactory
    private lateinit var viewModel:TvShowsViewModel
    private lateinit var adapter:MyTvShowsItemsRecyclerViewAdapter
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_tv_shows)
        factory=TvShowsViewModelFactory(TvShowsRepository("4da2ce878649bbad5b1ad09442ef2095"))
        viewModel=ViewModelProvider(this,factory)[TvShowsViewModel::class.java]

       binding.RecyclerViewTvShows.layoutManager=LinearLayoutManager(this)

        viewModel.getTvShow().observe(this) {
         adapter= MyTvShowsItemsRecyclerViewAdapter(it,this@TvShowsActivity)
            binding.viewProgressbar.isVisible=false
            binding.RecyclerViewTvShows.adapter=adapter
        }

    }
}