package com.example.tmdbapplication.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.tmdbapplication.R
import com.example.tmdbapplication.adapter.MyPopulerPersonRecyclerViewAdapter
import com.example.tmdbapplication.databinding.ActivityPopulerPersonBinding
import com.example.tmdbapplication.factory.PopulerPersonActivityFactory
import com.example.tmdbapplication.repository.PopulerPersonRepository
import com.example.tmdbapplication.viewModel.PopulerPersonActivityViewModel

class PopulerPersonActivity : AppCompatActivity() {
    private lateinit var binding:ActivityPopulerPersonBinding
    private lateinit var factory : PopulerPersonActivityFactory
    private lateinit var viewModel:PopulerPersonActivityViewModel
    private lateinit var adapter : MyPopulerPersonRecyclerViewAdapter
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_populer_person)
        factory= PopulerPersonActivityFactory(PopulerPersonRepository("4da2ce878649bbad5b1ad09442ef2095"))
        viewModel=ViewModelProvider(this,factory)[PopulerPersonActivityViewModel::class.java]

        binding.viewRecycler.layoutManager= StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.HORIZONTAL)

        viewModel.getPopulerPerson().observe(this) {
            adapter= MyPopulerPersonRecyclerViewAdapter(it,this@PopulerPersonActivity)
           binding.viewProgressbar.isVisible=false
            binding.viewRecycler.adapter=adapter
            binding.notifyChange()
        }
    }
}