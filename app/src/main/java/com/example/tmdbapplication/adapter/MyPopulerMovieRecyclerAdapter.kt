package com.example.tmdbapplication.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.tmdbapplication.R
import com.example.tmdbapplication.activity.ShowParticulerMovieDataActivity
import com.example.tmdbapplication.databinding.LayoutMovieItemBinding
import com.example.tmdbapplication.model.Movie
import com.example.tmdbapplication.utility.Keys
import com.google.gson.Gson
import com.squareup.picasso.Picasso

class MyPopulerMovieRecyclerAdapter(private val movieData : List<Movie>,private val context:Context) : RecyclerView.Adapter<MyPopulerMovieRecyclerAdapter.MyViewHolder>() {

    inner class MyViewHolder(var binding:LayoutMovieItemBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View? = LayoutInflater.from(parent.context).inflate(R.layout.layout_movie_item, parent, false)
        val binding: LayoutMovieItemBinding = DataBindingUtil.bind(view!!)!!
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return movieData.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = movieData[position]

        holder.binding.apply {
            tvMovieName.text=movie.originalTitle
            val urlPath="https://image.tmdb.org/t/p/w500/${movie.posterPath}"
            Picasso.get()
                .load(urlPath)
                .placeholder(R.drawable.baseline_movie_24)
                .error(R.drawable.baseline_error_24)
                .into(imgImageView)
            this.imgImageView.setOnClickListener{
                val intent=Intent(context,
                    ShowParticulerMovieDataActivity::class.java)
                intent.putExtra(Keys.MOVIEOBJECT, Gson().toJson(movie))
                context.startActivity(intent)
            }
        }
    }

}