package com.example.tmdbapplication.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.tmdbapplication.R
import com.example.tmdbapplication.activity.PersonViewInfoActivity
import com.example.tmdbapplication.databinding.LayoutPopulerPersonItemsBinding
import com.example.tmdbapplication.model.PopulerPersonObject
import com.example.tmdbapplication.utility.Keys
import com.google.gson.Gson
import com.squareup.picasso.Picasso

class MyPopulerPersonRecyclerViewAdapter(private val populerPersonData : List<PopulerPersonObject>,private val context:Context) : RecyclerView.Adapter<MyPopulerPersonRecyclerViewAdapter.MyViewHolder>() {

    inner class MyViewHolder(var binding: LayoutPopulerPersonItemsBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View? = LayoutInflater.from(parent.context).inflate(R.layout.layout_populer_person_items, parent, false)
        val binding: LayoutPopulerPersonItemsBinding = DataBindingUtil.bind(view!!)!!
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return populerPersonData.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val person = populerPersonData[position]

        holder.binding.apply {
//            tvPersonName.text=person.name
            val urlPath="https://image.tmdb.org/t/p/w500/${person.profilePath}"
            Picasso.get()
                .load(urlPath)
                .placeholder(R.drawable.baseline_movie_24)
                .error(R.drawable.baseline_error_24)
                .into(imgPersonImageView)
            this.imgPersonImageView.setOnClickListener{
                val intent= Intent(context,
                   PersonViewInfoActivity::class.java)
                intent.putExtra(Keys.POPULERPERSON, Gson().toJson(person))
                context.startActivity(intent)
            }
        }
    }

}