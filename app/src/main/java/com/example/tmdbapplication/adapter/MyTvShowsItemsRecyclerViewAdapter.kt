package com.example.tmdbapplication.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.tmdbapplication.R
import com.example.tmdbapplication.activity.ShowParticulerTvShowDataActivity
import com.example.tmdbapplication.databinding.LayoutTvshowsItemBinding
import com.example.tmdbapplication.model.TvShowItems
import com.example.tmdbapplication.utility.Keys
import com.google.gson.Gson
import com.squareup.picasso.Picasso

class MyTvShowsItemsRecyclerViewAdapter (private val tvShowData : List<TvShowItems>, private val context: Context) : RecyclerView.Adapter<MyTvShowsItemsRecyclerViewAdapter.MyViewHolder>() {

    inner class MyViewHolder(var binding: LayoutTvshowsItemBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View? = LayoutInflater.from(parent.context).inflate(R.layout.layout_tvshows_item, parent, false)
        val binding: LayoutTvshowsItemBinding = DataBindingUtil.bind(view!!)!!
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return tvShowData.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val tvShow = tvShowData[position]

        holder.binding.apply {
         //   tvTvShowName.text=tvShow.name
            val urlPath="https://image.tmdb.org/t/p/w500/${tvShow.posterPath}"
            Picasso.get()
                .load(urlPath)
                .placeholder(R.drawable.baseline_movie_24)
                .error(R.drawable.baseline_error_24)
                .into(imgTvShowsView)
            this.imgTvShowsView.setOnClickListener{
                val intent=Intent(context,ShowParticulerTvShowDataActivity::class.java)
                intent.putExtra(Keys.TVSHOWKEY,Gson().toJson(tvShow))
                context.startActivity(intent)
            }
        }
    }

}